package com.company.repositories;

import com.company.api.repositories.StaffRepository;
import com.company.model.Master;

import java.util.ArrayList;

public class StaffRepositoryImpl implements StaffRepository {

    private static StaffRepository instance;
    private ArrayList<Master> masters;

    private StaffRepositoryImpl() {
    }

    public static StaffRepository getInstance() {
        if (instance == null) {
            instance = new StaffRepositoryImpl();
        }

        return instance;
    }

    public ArrayList<Master> getMasters() {
        return masters;
    }

    public void setMasters(ArrayList<Master> masters) {
        this.masters = masters;
    }
}
