package com.company.repositories;

import com.company.api.repositories.OrderListRepository;
import com.company.model.Order;

import java.util.ArrayList;

public class OrderListRepositoryImpl implements OrderListRepository {

    private static OrderListRepositoryImpl instance;
    private ArrayList<Order> orders;


    private OrderListRepositoryImpl() {
    }

    public static OrderListRepositoryImpl getInstance() {
        if (instance == null) {
            instance = new OrderListRepositoryImpl();
        }

        return instance;
    }

    public ArrayList<Order> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<Order> orders) {
        this.orders = orders;
    }
}
