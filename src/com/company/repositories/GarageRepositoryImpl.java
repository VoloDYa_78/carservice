package com.company.repositories;

import com.company.api.repositories.GarageRepository;
import com.company.model.GarageSpace;

import java.util.ArrayList;

public class GarageRepositoryImpl implements GarageRepository {

    private ArrayList<GarageSpace> garageSpaces;
    private static GarageRepositoryImpl instance;


    private GarageRepositoryImpl() {
    }

    public static GarageRepositoryImpl getInstance() {
        if (instance == null) {
            instance = new GarageRepositoryImpl();
        }

        return instance;
    }


    public ArrayList<GarageSpace> getGarageSpaces() {
        return garageSpaces;
    }

    public void setGarageSpaces(ArrayList<GarageSpace> garageSpaces) {
        this.garageSpaces = garageSpaces;
    }
}
