package com.company.services;

import com.company.api.services.OrderManagerService;
import com.company.repositories.OrderListRepositoryImpl;
import com.company.model.Order;
import com.company.model.enums.OrderStatus;

public class OrderManagerServiceImpl implements OrderManagerService {

    private static OrderManagerServiceImpl instance;

    private OrderManagerServiceImpl() {
    }

    public static OrderManagerServiceImpl getInstance() {
        if (instance == null) {
            instance = new OrderManagerServiceImpl();
        }

        return instance;
    }

    public void AddOrderInOrderList(Order order) {
        OrderListRepositoryImpl.getInstance().getOrders().add(order);
    }

    public void DeleteOrderFromOrderListById(int orderId) {

        for (Order ord : OrderListRepositoryImpl.getInstance().getOrders()) {
            if (ord.getId() == orderId) {
                OrderListRepositoryImpl.getInstance().getOrders().remove(ord);
                return;
            }
        }
    }

    public void ChangeOrderStatus(int orderId, OrderStatus orderStatus) {
        for (Order order : OrderListRepositoryImpl.getInstance().getOrders()) {
            if (order.getId() == orderId) {
                order.setStatus(orderStatus);
            }
        }
    }

    public void ShiftOrderTime(long orderId, int shiftingTime) {
        for (Order ord : OrderListRepositoryImpl.getInstance().getOrders()) {
            if (ord.getId() == orderId) {
                int nowDeadline = ord.getDeadlineTime();
                ord.setDeadlineTime(nowDeadline + shiftingTime);
            }
        }
    }

}
