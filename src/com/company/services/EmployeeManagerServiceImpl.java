package com.company.services;

import com.company.api.services.EmployeeManagerService;
import com.company.model.Order;
import com.company.model.Master;
import com.company.repositories.OrderListRepositoryImpl;
import com.company.repositories.StaffRepositoryImpl;

public class EmployeeManagerServiceImpl implements EmployeeManagerService {

    private static EmployeeManagerServiceImpl instance;

    private EmployeeManagerServiceImpl() {
    }

    public static EmployeeManagerServiceImpl getInstance() {
        if (instance == null) {
            instance = new EmployeeManagerServiceImpl();
        }

        return instance;
    }


    public void AddMasterToStaff(Master master) {
        StaffRepositoryImpl.getInstance().getMasters().add(master);
    }

    public void AddMasterToOrderFromStaff(int masterId, int orderId) {
        for (Master master : StaffRepositoryImpl.getInstance().getMasters()) {
            if (master.getId() == masterId) {
                for (Order order : OrderListRepositoryImpl.getInstance().getOrders()) {
                    if (order.getId() == orderId) {
                        order.setMaster(master);
                    }
                }
                return;
            }
        }
    }
}
