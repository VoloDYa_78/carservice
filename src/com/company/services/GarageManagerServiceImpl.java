package com.company.services;

import com.company.api.services.GarageManagerService;
import com.company.model.GarageSpace;
import com.company.repositories.GarageRepositoryImpl;

public class GarageManagerServiceImpl implements GarageManagerService {

    private static GarageManagerServiceImpl instance;

    private GarageManagerServiceImpl() {
    }

    public static GarageManagerServiceImpl getInstance() {
        if (instance == null) {
            instance = new GarageManagerServiceImpl();
        }

        return instance;
    }

    public void AddSpaceToGarage(GarageSpace garageSpace) {
        GarageRepositoryImpl.getInstance().getGarageSpaces().add(garageSpace);
    }

    public void RemoveSpaceFromGarage(long spaceId) {
        for (GarageSpace garageSpace : GarageRepositoryImpl.getInstance().getGarageSpaces()) {
            if (garageSpace.getId() == spaceId) {
                GarageRepositoryImpl.getInstance().getGarageSpaces().remove(garageSpace);
            }
        }
    }
}
