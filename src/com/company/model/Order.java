package com.company.model;

import com.company.model.enums.OrderStatus;
import com.company.utils.IdUtil;

public class Order extends BaseEntity {

    private int deadlineTime;
    private OrderStatus orderStatus;
    private String carName;
    private GarageSpace garageSpace;
    private Master master;


    public Order(int deadlineTime, OrderStatus orderStatus, String carName, GarageSpace garageSpace, Master master) {
        super(IdUtil.getInstance().generateOrderId());
        this.deadlineTime = deadlineTime;
        this.orderStatus = orderStatus;
        this.carName = carName;
        this.garageSpace = garageSpace;
        this.master = master;
    }


    public int getDeadlineTime() {
        return deadlineTime;
    }

    public OrderStatus getStatus() {
        return orderStatus;
    }

    public String getCarName() {
        return carName;
    }

    public GarageSpace getGarageSpace() {
        return garageSpace;
    }

    public Master getMaster() {
        return master;
    }

    public void setDeadlineTime(int deadlineTime) {
        this.deadlineTime = deadlineTime;
    }

    public void setStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public void setGarageSpace(GarageSpace garageSpace) {
        this.garageSpace = garageSpace;
    }

    public void setMaster(Master master) {
        this.master = master;
    }
}
