package com.company.model.enums;

public enum SpaceStatus {
    EMPTY,
    FULL
}
