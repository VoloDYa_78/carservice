package com.company.model.enums;

public enum OrderStatus {
    COMPLETED,
    CANCELED
}
