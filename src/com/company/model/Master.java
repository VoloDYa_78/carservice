package com.company.model;

import com.company.utils.IdUtil;

public class Master extends BaseEntity {

    private String name;

    public Master(String name) {
        super(IdUtil.getInstance().generateMasterId());
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
