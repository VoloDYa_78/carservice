package com.company.model;

import com.company.model.enums.SpaceStatus;
import com.company.utils.IdUtil;

public class GarageSpace extends BaseEntity {

    private SpaceStatus status;

    public GarageSpace(SpaceStatus status) {
        super(IdUtil.getInstance().generateGarageId());
        this.status = status;
    }

    public SpaceStatus getStatus() {
        return status;
    }

    public void setStatus(SpaceStatus status) {
        this.status = status;
    }
}
