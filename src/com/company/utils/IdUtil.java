package com.company.utils;

public class IdUtil {
    private static IdUtil instance;
    private static Long order_id_sequence = -1L;
    private static Long master_id_sequence = -1L;
    private static Long garage_id_sequence = -1L;


    private IdUtil() { }

    public static IdUtil getInstance() {
        if (instance == null) {
            instance = new IdUtil();
        }

        return instance;
    }

    public Long generateOrderId(){return ++order_id_sequence;}
    public Long generateMasterId(){return ++master_id_sequence;}
    public Long generateGarageId() {
        return ++garage_id_sequence;
    }

}
