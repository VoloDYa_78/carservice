package com.company.api.services;

import com.company.model.Master;

public interface EmployeeManagerService {

    public void AddMasterToStaff(Master master);
    public void AddMasterToOrderFromStaff(int masterId, int orderId);
}
