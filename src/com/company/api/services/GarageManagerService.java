package com.company.api.services;

import com.company.model.GarageSpace;

public interface GarageManagerService {

    public void AddSpaceToGarage(GarageSpace garageSpace);
    public void RemoveSpaceFromGarage(long spaceId);
}
