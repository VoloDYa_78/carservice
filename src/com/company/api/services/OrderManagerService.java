package com.company.api.services;

import com.company.model.Order;
import com.company.model.enums.OrderStatus;

public interface OrderManagerService {

    public void AddOrderInOrderList(Order order);
    public void DeleteOrderFromOrderListById(int orderId);
    public void ChangeOrderStatus(int orderId, OrderStatus orderStatus);
    public void ShiftOrderTime(long orderId, int shiftingTime);
}
