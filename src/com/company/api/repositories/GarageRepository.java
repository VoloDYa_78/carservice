package com.company.api.repositories;

import com.company.model.GarageSpace;

import java.util.ArrayList;

public interface GarageRepository {

    public ArrayList<GarageSpace> getGarageSpaces();
    public void setGarageSpaces(ArrayList<GarageSpace> garageSpaces);
}
