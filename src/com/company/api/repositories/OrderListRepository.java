package com.company.api.repositories;

import com.company.model.Order;

import java.util.ArrayList;

public interface OrderListRepository {

    public ArrayList<Order> getOrders();
    public void setOrders(ArrayList<Order> orders);
}
